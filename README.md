# MockUtils
Handy-dandy utilities for unit testing, test isolation, dependency injection, etc.

https://medium.com/@bdp.inbox/python-dependency-injection-for-lazy-people-5832aeaa7522

## Usage
### Mocking a function
```
from mockutils.MockUtils import MockUtils

def mockFoo():
    return 0
    
MockUtils.mockModule('my.dependency').foo = mockFoo

import my.class.to.test
...
```
### Isolating a test (mostly)
```
from mockutils.IsolatedTest import IsolatedTest
from mockutils.MockUtils import MockUtils
import unittest

class test_MyClass(IsolatedTest):
    '''
    This test class will automatically clean up its mess of mocks when finished.
    It will also clean up mocks before running itself, just in case another test
    made a mess.
    '''
    
    def setUp(self):
        '''
        Uh oh, another test already imported this module without any mocks!
        So we delete it, create our mock classes, then import it again.
        Now when we do the import, the class will see our mocks as its dependencies.
        '''
        MockUtils.deleteImportedModule('my.class.to.test')
        
        # Mock stuff
        
        # Import stuff to test
```