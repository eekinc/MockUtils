import importlib
from mockutils.MockUtils import MockUtils
import os
import sys
import unittest

class IsolatedTest(unittest.TestCase):
    '''
    Test case which automatically cleans up mocks before and after running tests.
    '''
    _modules = None
    _REFERENCE_MODULE_NAMES = set([moduleName for moduleName, _ in sys.modules.items()])

    @classmethod
    def _getAllModuleNames(cls):
        return sorted([moduleName for moduleName, _ in sys.modules.items()])

    @classmethod
    def setUpClass(cls):
        # print('\nSetup: {}'.format(cls.__name__))

        allModules = cls._getAllModuleNames()
        IsolatedTest._modules = set(allModules)
        IsolatedTest._numModules = len(allModules)

        for moduleName in allModules:
            if not moduleName in cls._REFERENCE_MODULE_NAMES:
                pass
                # print('WARNING: Extra module {} found in {}.'.format(moduleName, cls.__name__))

        MockUtils.resetMockedModules()

    @classmethod
    def _warnLeftoverModules(cls):
        dirs = set(os.listdir())

        for moduleName in cls._getAllModuleNames():
            if moduleName.startswith('test') or moduleName in cls._modules:
                continue

            first = moduleName.split('.')[0]

            if first in dirs:
                message = 'WARNING: Module {} may be left over from {}.'.format(moduleName, cls.__name__)
                if moduleName in cls._modules:
                    message = message + ' Module found in reference list.';
                # print(message)

    @classmethod
    def tearDownClass(cls):
        # print('\nTear down: {}'.format(cls.__name__))

        for moduleName in cls._getAllModuleNames():
            if not moduleName in IsolatedTest._modules:
                MockUtils.deleteImportedModule(moduleName, False)

        MockUtils.resetMockedModules()

        importlib.invalidate_caches()

        allModules = cls._getAllModuleNames()
        numModules = len(allModules)
        if numModules > IsolatedTest._numModules:
            pass
            # print('ERROR: Expected {} modules, but found {} in {}.'.format(cls._numModules, numModules, cls.__name__))

        cls._warnLeftoverModules()
