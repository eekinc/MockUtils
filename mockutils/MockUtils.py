'''
Unit testing mock utilities for Python 3.
'''

from mockutils.ModuleNameIterator import ModuleNameIterator
import imp, importlib, sys

class MockUtils:
    _mockedModules = []

    @staticmethod
    def deleteImportedModule(moduleName, warn=True):
        didSomething = False

        if moduleName in sys.modules:
            module = sys.modules[moduleName]
            sys.modules[moduleName] = None
            del module
            del sys.modules[moduleName]

            didSomething = True

        if warn and not didSomething:
            print('Warning: deleteImportedModule(\'{}\') was a no-op.'.format(moduleName))

    @staticmethod
    def mockModule(moduleName, replacementModule=None, overwritePrefix=True):
        if replacementModule is None:
            module = imp.new_module(moduleName)
        else:
            module = replacementModule

        if overwritePrefix:
            for modulePartName in ModuleNameIterator(moduleName):
                MockUtils.deleteImportedModule(modulePartName, False)
        else:
            MockUtils.deleteImportedModule(moduleName, False)
        
        sys.modules[moduleName] = module
        MockUtils._mockedModules.append((moduleName, module))

        importlib.invalidate_caches()
        
        return module

    @staticmethod
    def resetMockedModules():
        for moduleName, module in MockUtils._mockedModules:
            MockUtils.deleteImportedModule(moduleName, False)

        MockUtils._mockedModules = []
