class ModuleNameIterator:
    def __init__(self, moduleName):
        self._moduleName = moduleName
        self._i = 0

    def __iter__(self):
        return self

    def __next__(self):
        moduleParts = self._moduleName.split('.')

        modulePartName = '.'.join(moduleParts[:self._i])

        if self._i >= len(modulePartName):
            raise StopIteration()

        self._i += 1

        return modulePartName