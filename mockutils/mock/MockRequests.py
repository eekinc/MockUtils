from datetime import timedelta
import json
from unittest.mock import MagicMock

class MockRequests:
    '''
    Mocks the requests module.
    '''

    class Response:
        '''
        Mock requests.Response.
        '''

        RETURN_JSON = {
            'Data': [],
            'Total': 0
        }
        
        def __init__(self, value='{}', *args, **kwargs):
            self.status_code = 200
            self._value = value
            
            # Support download manifest.
            self.iter_content = lambda **kwargs: []
            
            # These are used to test logging.
            self.history = None
            self.request = MagicMock()
            self.request.method = 'METHOD'
            self.request.url = 'URL'
            self.status_code = 200
            self.reason = 'OK'
            self.elapsed = timedelta(seconds=1, milliseconds=420)
            self.request.headers = {}
            self.request.body = 'body'
            self.headers = {}
            self.text = str(value)

        def json(self):
            try:
                return json.loads(self._value)
            except:
                raise ValueError('No JSON object could be decoded.')
        
    def __init__(self):
        self._response = []

    def addResponse(self, response):
        self._response.append(response)
    
    def get(self, url, **kwargs):
        return self._response.pop(0) if self._response else self.__class__.Response()
    
    def post(self, url, data=None, json=None, **kwargs):
        return self._response.pop(0) if self._response else self.__class__.Response()

MockRequests.Session = MockRequests